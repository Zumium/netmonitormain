/*
程序主循环，是本软件最重要的部分之一
主循环包括三部分：
1. 从NodeDiscoveror获取当前在线的节点列表
2. 将节点列表交给Measure模块进行测量
3. 将Measure的测量结果同时传递给WebBackend和DataManager，
	 WebBackend将结果推送给需要的客户端，
	 DataManager则将该结果作为一条新纪录存入数据库中

除主循环外，还包括一个定时器，用于周期性地进行大循环，驱动整个程序的运转
*/
const Promise=require('bluebird');
const request=Promise.promisifyAll(require('request'),{multiArgs:true});
const _=require('underscore');
const config=require('../config/config');

var loop=null;

exports.up=()=>{
		loop=setInterval(measure,config.Measure.measureInterval*1000);
		measure();
		return true;
}

exports.down=()=>clearInterval(loop);

const measure=()=>{
	/*
	 * Measurement starts here
	 */
	var addrToIface={};

	return Promise.resolve()
	.then(
		()=>request.getAsync({
			//uri:'http://unix:'+config.NodeDiscoveror.serverSocket+':/nodes',
			uri:getUri('NodeDiscoveror','/nodes'),
			json:true
		})
	)
	.then(
		nodesRslt=>{
			var rsltJson=nodesRslt[nodesRslt.length-1];
			rsltJson.forEach(each=>addrToIface[each.address]=each['interface']);
			return rsltJson.map(each=>each.address);
		}
	)
	.then(
		addresses=>new Promise((resolve,reject)=>
			request
			.post({
				//uri:'http://unix:'+config.Measure.serverSocket+':/measure',
				uri:getUri('Measure','/measure'),
				json:true,
				body:{addresses:addresses}
			},
			(error,response,body)=>{
				if(error)
					return reject(error);
				if(response.statusCode!==200)
					return reject(new Error('StatusCode:'+response.statusCode+'   '+response.statusMessage));
				resolve(body);
			})
		)
	)
	.then(
		msrRsltJson=>{
			var rsltWithIface=msrRsltJson.map(each=>{
				each['interface']=addrToIface[each.address];
				return each;
			});
			var toDataManagerPromises=rsltWithIface.map(
				each=>new Promise((resolve,reject)=>
					request
					.post({
						//uri:'http://unix:'+config.DataManager.serverSocket+':/',
						uri:getUri('DataManager','/'),
						json:true,
						body:each
					},
					(error,response,body)=>{
						if(error)
							return reject(error);
						if(response.statusCode!==201)
							return reject(new Error('StatusCode:'+response.statusCode+'   '+response.statusMessage));
						resolve();
					})
				)
			);
			var toWebBackendPromises=rsltWithIface.map(
				each=>new Promise((resolve,reject)=>
					request
					.post({
						//uri:'http://unix:'+config.WebBackend.serverSocket+':/data',
						uri:getUri('WebBackend','/data'),
						json:true,
						body:each
					},
					(error,response,body)=>{
						if(error)
							return reject(error);
						if(response.statusCode!==200)
							return reject(new Error('StatusCode:'+response.statusCode+'   '+response.statusMessage));
						resolve();
					})
				)
			);

			return Promise.all(toDataManagerPromises.concat(toWebBackendPromises));
		}
	);
};

const getUri=(component,path)=>'http://unix:'+config[component].serverSocket+':'+path;
