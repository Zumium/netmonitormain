const express=require('express');
const bodyParser=require('body-parser');
const config=require('../config/config');
const moduleManager=require('./modules');
const _=require('underscore');

var router=module.exports=express.Router();
router.use(bodyParser.json());

router.get('/config/WebBackend',(req,res,next)=>{
	var configToWebBackend=_.clone(config.WebBackend);
	configToWebBackend.componentAddrs=_.mapObject(_.pick(config,'NodeDiscoveror','DataManager','Measure'),val=>val.serverSocket);
	res.json(configToWebBackend);
});

router.get('/config/:component',(req,res,next)=>{
	if(config[req.params.component])
		res.json(config[req.params.component]);
	else
		res.sendStatus(404);
});

router.post('/moduleready',(req,res,next)=>{
	moduleManager.moduleReady(req.body.moduleName);
	res.sendStatus(200);
});
