const Promise=require('bluebird');
const config=require('../config/config');
const fork=require('child_process').fork;
const path=require('path');
const ev=require('events');
const _=require('underscore');
const request=require('request');

var subprocesses={
	NodeDiscoveror:null,
	Measure:null,
	DataManager:null,
	WebBackend:null
};

var startupEv=new ev();

const STATUS_DISABLED=0;
const STATUS_STARTING=1;
const STATUS_UP=2;
startupEv.status={
	NodeDiscoveror: STATUS_DISABLED,
	Measure: STATUS_DISABLED,
	DataManager: STATUS_DISABLED,
	WebBackend: STATUS_DISABLED
};
startupEv.on('module-ready',moduleName=>{
	const enabledModulesCount=_.keys(_.omit(startupEv.status,val=>val===STATUS_DISABLED)).length;
	if((startupEv.status)[moduleName]===STATUS_STARTING){
		(startupEv.status)[moduleName]=STATUS_UP;
		if(_.keys(_.pick(startupEv.status,val=>val===STATUS_UP)).length===enabledModulesCount)
			startupEv.emit('modules-up');
	}
});

exports.up=options=>new Promise((resolve,reject)=>{
	startupEv.once('modules-up',resolve);
	startupEv.on('error',reject);

	if(!options['no-measure'])
		startupEv.status.Measure=STATUS_STARTING;
	if(!options['no-data-manager'])
		startupEv.status.DataManager=STATUS_STARTING;
	if(!options['no-node-discoveror'])
		startupEv.status.NodeDiscoveror=STATUS_STARTING;
	if(!options['no-web-backend'])
		startupEv.status.WebBackend=STATUS_STARTING;

	_.keys(_.pick(startupEv.status,val=>val===STATUS_STARTING)).map(
		moduleName=>subprocesses[moduleName]=fork(
			appJsPath(moduleName),
			[config.NetmonitorMain.serverSocket],
			{cwd:modulePath(moduleName)}
		)
	);

});

exports.down=()=>new Promise((resolve,reject)=>{
	var closePromises=_.keys(_.pick(startupEv.status,val=>val===STATUS_UP)).map(
		moduleName=>new Promise(
			(resolve,reject)=>
				request.delete(getUri(moduleName,'/'),
					(err,response)=>{
						if(err) return reject(err);
						if(response.statusCode !== 200) return reject(new Error('Closing '+moduleName+' failed'));
						resolve();
					}
				)
		)
	);
	Promise.all(closePromises).then(resolve,reject);
});

exports.moduleReady=moduleName=>startupEv.emit('module-ready',moduleName);

const modulePath=moduleName=>path.resolve(__dirname,'..',config[moduleName].path);
const appJsPath=moduleName=>path.resolve(modulePath(moduleName),'app.js');

const getUri=(moduleName,path)=>'http://unix:'+config[moduleName].serverSocket+':'+path;
