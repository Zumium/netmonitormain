/*
本文件是主模块NetmonitorMain的入口文件
NetmonitorMain模块首先启动自身
当自身启动完成后，通过子进程的方式启动其余四个模块
所有模块均启动完成后在屏幕上输出Boot sequence completed文字，则启动过程完成，程序进入正常运转
*/
const Promise=require('bluebird');
const express=require('express');
const cmdArgs=require('command-line-args');
const fs=Promise.promisifyAll(require('fs'));
const colors=require('colors/safe');
const config=require('./config/config').NetmonitorMain;
const router=require('./components/router');
const moduleManager=require('./components/modules');
const loop=require('./components/loop');

/*
命令行flag
可关闭部分功能，方便调试
*/
const cmdOptions=[
	{name:'no-loop',type:Boolean},
	{name:'no-node-discoveror',type:Boolean},
	{name:'no-measure',type:Boolean},
	{name:'no-data-manager',type:Boolean},
	{name:'no-web-backend',type:Boolean}
];
const options=cmdArgs(cmdOptions);

const app=express();

app.use(router);

fs.accessAsync(config.serverSocket,fs.constants.F_OK)
.then(
	()=>fs.unlinkAsync(config.serverSocket),
	err=>Promise.resolve()
)
.then(
	()=>new Promise((resolve,reject)=>
		app.listen(config.serverSocket,()=>{
			console.log(colors.green('=> Main module started, starting other modules'));
			moduleManager.up(options).then(resolve,reject);
		})
	)
)
.then(
	()=>{
		console.log(colors.green('=> All modules started'));
		if(options['no-loop']) return false;
		console.log(colors.green('=> Main loop starting'));
		return loop.up();
	}
)
.then(
	isStartingLoop=>{
		if(isStartingLoop)
			console.log(colors.green('=> Main loop started'));
			console.log(colors.grey('=> Booting sequence completed'));
	}
);

process.on('SIGQUIT',
	()=>moduleManager
				.down()
				.then(()=>{
					console.log(colors.grey('NetMonitor and submodules have stopped'));
					process.exit();
				})
);
